/* sm.c - Bluetooth SPI slave application state machine
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sdk_config.h"
#include "app_util_platform.h"
#include "app_timer.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_spis.h"
#include "nrf_gpio.h"
#include "boards.h"
#include "app_error.h"
#include <string.h>

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_delay.h"

#include "spis_regs.h"
#include "buffer.h"
#include "sm.h"
#include "firmware.h"

#include "ble_serv.h"

/* Static variables */
static sm_state_t m_state = SM_STATE_BOOT;
static uint8_t m_mode = NRF52_MODE_IDLE;

static buffer_t m_rx_data_port;
static volatile uint8_t  m_rx_data[RX_FIFO_SIZE];
static buffer_t m_tx_data_port;
static volatile uint8_t  m_tx_data[TX_FIFO_SIZE];
static volatile uint8_t  m_tx_packet[MAX_PACKET_SIZE];
static volatile uint8_t  m_tx_packet_pending = 0;
static volatile uint8_t  m_rx_overflow_count = 0;

static volatile uint32_t m_fw_upgrade_size;
static volatile uint32_t m_fw_upgrade_crc;

static volatile uint8_t m_int_status = 0;
static volatile uint8_t m_int_enable = 0;

static volatile uint8_t m_error_code = 0;

static inline volatile void * memcpy_volatile(volatile void *dest, volatile void *src, size_t len)
{
    volatile char *d = dest;
    volatile char *s = src;
    while (len--)
        *d++ = *s++;
    return dest;
}

/* Static functions */
static uint32_t copy_port_to_linear_buffer(buffer_t *port, volatile uint8_t *linear_buffer, uint32_t len)
{
    uint32_t avail, actual_len = 0;
    uintptr_t src;
    do
    {
        avail = buffer_read(port, &src);
        avail = MIN(avail, len);
        memcpy_volatile(linear_buffer, (void *)src, avail);
        buffer_read_advance(port, avail);
        linear_buffer += avail;
        len -= avail;
        actual_len += avail;
    } while (avail > 0 && len > 0);

    return actual_len;
}

static uint32_t copy_linear_buffer_to_port(buffer_t *port, volatile uint8_t *linear_buffer, uint32_t len)
{
    uint32_t avail, actual_len = 0;
    uintptr_t dest;
    do
    {
        avail = buffer_write(port, &dest);
        avail = MIN(avail, len);
        memcpy_volatile((void *)dest, linear_buffer, avail);
        buffer_write_advance(port, avail);
        linear_buffer += avail;
        len -= avail;
        actual_len += avail;
    } while (avail > 0 && len > 0);

    return actual_len;
}

static uint16_t read_app_version_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    static const uint16_t app_version = 0x0001;
    memcpy(buffer, &app_version, sizeof(app_version));
    NRF_LOG_DEBUG("read_app_version_request: ver=%04x", app_version);
    return sizeof(app_version);
}

static uint16_t read_soft_dev_version_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    uint16_t soft_dev_version = ble_get_soft_device_version();
    memcpy(buffer, &soft_dev_version, sizeof(soft_dev_version));
    NRF_LOG_DEBUG("read_soft_dev_version_request: ver=%04x", soft_dev_version);
    return sizeof(soft_dev_version);
}

static uint16_t read_mode_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(buffer, &m_mode, sizeof(m_mode));
    NRF_LOG_DEBUG("read_mode_request: m_mode=%02x", m_mode);
    return sizeof(m_mode);
}

static void write_mode_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    m_mode = buffer[0];
    NRF_LOG_INFO("write_mode_complete: m_mode=%02x", m_mode);
}

static void write_fw_upgrade_size_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy_volatile(&m_fw_upgrade_size, buffer, sizeof(m_fw_upgrade_size));
    NRF_LOG_DEBUG("write_fw_upgrade_size_complete: m_fw_upgrade_size=%08x", m_fw_upgrade_size);
}

static void write_fw_upgrade_crc_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy_volatile(&m_fw_upgrade_crc, buffer, sizeof(m_fw_upgrade_crc));
    NRF_LOG_DEBUG("write_fw_upgrade_crc_complete: m_fw_upgrade_crc=%08x", m_fw_upgrade_crc);
}

static uint16_t read_int_status_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy_volatile(buffer, &m_int_status, sizeof(m_int_status));
    NRF_LOG_DEBUG("read_int_status_request: m_int_status=%02x", m_int_status);
    return sizeof(m_int_status);
}

static void write_int_enable_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    m_int_enable = buffer[0];
    NRF_LOG_DEBUG("write_int_enable_complete: m_int_enable=%02x", m_int_enable);
}

static uint16_t read_error_code_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy_volatile(buffer, &m_error_code, sizeof(m_error_code));
    NRF_LOG_DEBUG("read_error_code_request: m_error_code=%02x", m_error_code);
    return sizeof(m_error_code);
}

static uint16_t read_device_address_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    ble_get_device_address(buffer, NRF52_REG_SIZE_DEVICE_ADDRESS);
    NRF_LOG_DEBUG("read_device_address_request: address=0x%02x%02x%02x%02x%02x%02x", buffer[5], buffer[4], buffer[3], buffer[2], buffer[1], buffer[0]);
    return NRF52_REG_SIZE_DEVICE_ADDRESS;
}

static void write_device_address_complete(uint8_t *buffer, uint16_t length)
{
    ble_set_device_address(buffer, length);
    NRF_LOG_DEBUG("write_device_address_complete: address=0x%02x%02x%02x%02x%02x%02x", buffer[5], buffer[4], buffer[3], buffer[2], buffer[1], buffer[0]);
}

static uint16_t read_advertising_interval_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    uint16_t advertising_interval = ble_get_advertising_interval();
    memcpy_volatile(buffer, &advertising_interval, sizeof(advertising_interval));
    NRF_LOG_DEBUG("read_advertising_interval_request: advertising_interval=0x%04x", advertising_interval);
    return sizeof(advertising_interval);
}

static void write_advertising_interval_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    uint16_t advertising_interval = ((uint16_t) buffer[0]) | (((uint16_t) buffer[1] << 8) & 0xFF00);
    ble_set_advertising_interval(advertising_interval);
    NRF_LOG_DEBUG("write_advertising_interval_complete: advertising_interval=0x%04x", advertising_interval);
}

static uint16_t read_connection_interval_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    uint16_t connection_interval = ble_get_connection_interval();
    memcpy_volatile(buffer, &connection_interval, sizeof(connection_interval));
    NRF_LOG_DEBUG("read_connection_interval_request: connection_interval=0x%04x", connection_interval);
    return sizeof(connection_interval);
}

static void write_connection_interval_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    uint16_t connection_interval = ((uint16_t) buffer[0]) | (((uint16_t) buffer[1] << 8) & 0xFF00);
    ble_set_connection_interval(connection_interval);
    NRF_LOG_DEBUG("write_connection_interval_complete: connection_interval=0x%04x", connection_interval);
}

static uint16_t read_phy_mode_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    uint8_t phy = ble_get_preferred_phy();
    memcpy(buffer, &phy, sizeof(phy));
    NRF_LOG_DEBUG("read_phy_mode_request: phy=0x%u Mbps", phy);
    return sizeof(phy);
}

static void write_phy_mode_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    ble_set_preferred_phy(buffer[0] + 1); // Translate NRF52_PHY_MODE_1_MBPS to actual mbps
    NRF_LOG_DEBUG("write_phy_mode_complete: phy=0x%u Mbps", buffer[0]);
}

static uint16_t read_tx_data_length_request(uint8_t *buffer, uint16_t length)
{
    uint16_t tx_data_length;
    (void) length;
    tx_data_length = buffer_occupancy(&m_tx_data_port);
    memcpy(buffer, &tx_data_length, sizeof(tx_data_length));
    NRF_LOG_DEBUG("read_tx_data_length_request: tx_data_length=%04x", tx_data_length);
    return sizeof(tx_data_length);
}

static void write_tx_data_port_complete(uint8_t *buffer, uint16_t length)
{
    buffer_t *port;
    uint16_t total_size = 0;

    /* Put packets into RX data port if we are in loopback mode */
    if (m_mode == NRF52_MODE_LOOPBACK)
        port = &m_rx_data_port;
    else
        port = &m_tx_data_port;

    total_size = copy_linear_buffer_to_port(port, buffer, length);

    NRF_LOG_DEBUG("write_tx_data_port_complete: length=%u", total_size);
}

static uint16_t read_tx_fifo_size_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    uint16_t tx_fifo_size = TX_FIFO_SIZE;
    memcpy(buffer, &tx_fifo_size, sizeof(tx_fifo_size));
    NRF_LOG_DEBUG("read_tx_fifo_size_request: tx_fifo_size=0x%04x", tx_fifo_size);
    return sizeof(tx_fifo_size);
}

static uint16_t read_rx_data_length_request(uint8_t *buffer, uint16_t length)
{
    uint16_t rx_data_length;
    (void) length;
    rx_data_length = buffer_occupancy(&m_rx_data_port);
    memcpy(buffer, &rx_data_length, sizeof(rx_data_length));
    NRF_LOG_DEBUG("read_rx_data_length_request: rx_data_length=%04x", rx_data_length);
    return sizeof(rx_data_length);
}

static uint16_t read_rx_data_port_request(uint8_t *buffer, uint16_t length)
{
    uint16_t total_size = 0;

    total_size = copy_port_to_linear_buffer(&m_rx_data_port, buffer, length);

    NRF_LOG_DEBUG("read_rx_data_port_complete: length=%04x", total_size);

    return total_size;
}

static uint16_t read_rx_fifo_size_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    uint16_t rx_fifo_size = RX_FIFO_SIZE;
    memcpy(buffer, &rx_fifo_size, sizeof(rx_fifo_size));
    NRF_LOG_DEBUG("read_rx_fifo_size_request: rx_fifo_size=0x%04x", rx_fifo_size);
    return sizeof(rx_fifo_size);
}

static void wait_for_activity(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}

static inline void irq_clear(void)
{
    nrf_gpio_pin_set(API_SPIS_IRQ_PIN);
}

static inline void irq_set(void)
{
    nrf_gpio_pin_clear(API_SPIS_IRQ_PIN);
}

static void irq_init(void)
{
    nrf_gpio_cfg_output(API_SPIS_IRQ_PIN);
    irq_clear();
}

static void irq_process(void)
{
    if (m_int_enable & m_int_status)
    {
        irq_set();
        irq_clear();
    }
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module.
 */
static void timers_init(void)
{
    // Initialize timer module, making it use the scheduler
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

static void power_management_init(void)
{
    ret_code_t ret;
    ret = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(ret);
}

static void hard_reset(void)
{
    nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_RESET);
}

void ble_serv_data_in_notify(uint8_t *data, uint8_t len)
{
    /* Copy packet into the RX data port */
    NRF_LOG_DEBUG("gatt_data_in_handler: len=%u", len);

    (void)copy_linear_buffer_to_port(&m_rx_data_port, data, len);

    /* Handle data port overflow condition */
    uint8_t overflow_count = (uint8_t)buffer_overflows(&m_rx_data_port);
    if (overflow_count != m_rx_overflow_count)
    {
        m_int_status |= NRF52_INT_ERROR_INDICATION;
        m_error_code = NRF52_ERROR_OVERFLOW;
        m_rx_overflow_count = overflow_count;
        NRF_LOG_DEBUG("m_rx_overflow_count = %u", m_rx_overflow_count);
    }
    else
    {
        /* Raise RX data ready IRQ */
        m_int_status |= NRF52_INT_RX_DATA_READY;
    }

    irq_process();
}

static void service_tx_data_port(void)
{
    /* Keep sending packets until the stack is full or the data port
     * is empty.
     */
    while (true)
    {
        uint8_t to_send = m_tx_packet_pending ? m_tx_packet_pending : copy_port_to_linear_buffer(&m_tx_data_port, m_tx_packet, ble_get_max_payload_size());
        if (to_send)
        {
            if (ble_serv_send((uint8_t *) m_tx_packet, to_send) != NRF_SUCCESS)
            {
                m_tx_packet_pending = to_send;
                break;
            }

            m_tx_packet_pending = 0;
        }
        else
            break;
    }
}

void ble_serv_connection_notify(bool is_connected)
{
    /* Connection state has changed */
    NRF_LOG_INFO("ble_serv_connection_notify: %u", is_connected);

    buffer_reset(&m_tx_data_port);
    buffer_reset(&m_rx_data_port);
    m_tx_packet_pending = 0;

    if (is_connected)
    {
        m_int_status |= NRF52_INT_GATT_CONNECTED;
    }
    else
    {
        m_int_status &= ~NRF52_INT_GATT_CONNECTED;
    }
    irq_process();

    // TODO: we need to pass UUID of the connected device into
    // m_target_uuid.  This could be done using a new ble_serv
    // function call.
}

static void start_gatt_server(void)
{
    NRF_LOG_INFO("start_gatt_server");

    // Make sure our session is clean before starting
    m_int_status = 0;
    m_rx_overflow_count = 0;
    m_error_code = 0;

    ble_serv_start();
}

static void stop_gatt_server(void)
{
    NRF_LOG_INFO("stop_gatt_server");

    ble_serv_stop();

    m_int_status = 0;
    m_rx_overflow_count = 0;
    m_error_code = 0;
}

static void cleanup_current_state(void)
{
    /* Add any state-specific clean-up code here */
    switch (m_state)
    {
        case SM_STATE_GATT_SERVER:
        {
            stop_gatt_server();
        } break;
        default:
            break;
    }
}

static void apply_new_state(sm_state_t new_state)
{
    /* Add any state-specific entry code here */
    NRF_LOG_INFO("apply_new_state: %u -> %u", m_state, new_state);
    switch (new_state)
    {
        case SM_STATE_GATT_SERVER:
            start_gatt_server();
            break;

        default:
            break;
    }

    m_state = new_state;
}

static void transition_state(sm_state_t new_state)
{
    if (new_state == m_state)
        return;

    /* State change has been requested -- check for any clean-up operations */
    cleanup_current_state();

    /* Prepare new state */
    apply_new_state(new_state);
}

static void process_mode_change(void)
{
    /* Check to see if mode has changed */
    switch (m_mode)
    {
        case NRF52_MODE_GATT_SERVER:
            transition_state(SM_STATE_GATT_SERVER);
            break;
        case NRF52_MODE_FW_UPGRADE:
            transition_state(SM_STATE_FW_UPGRADE);
            break;
        case NRF52_MODE_RESET:
            hard_reset();
            break;
        case NRF52_MODE_LOOPBACK:
        case NRF52_MODE_IDLE:
        default:
            transition_state(SM_STATE_IDLE);
            break;
    }
}

static void sm_state_gatt_server(void)
{
    service_tx_data_port();
    process_mode_change();
    wait_for_activity();
}

__RAMFUNC void sm_state_fw_upgrade(void)
{
    if (FIRMWARE_NO_ERROR != firmware_prepare(m_fw_upgrade_size))
    {
        m_mode = SM_STATE_IDLE;
        process_mode_change();
        return;
    }

    firmware_write();

    firmware_finalise(); // Reset the device
}

static void sm_state_idle(void)
{
    wait_for_activity();

#ifdef AUTO_START_SERVER
    uint8_t mode = NRF52_MODE_GATT_SERVER;
    write_mode_complete((uint8_t *)&mode, 1);
#endif

    process_mode_change();
}

static void sm_state_boot(void)
{
    /* Setup timers */
    timers_init();

    /* Setup power management */
    power_management_init();

    /* Setup BLE but don't start it */
    ble_init();

    spis_reg_init();

    /* Install SPI register read/write handlers */
    spis_reg_install(NRF52_REG_ADDR_APP_VERSION, read_app_version_request, NULL, NULL);
    spis_reg_install(NRF52_REG_ADDR_SOFT_DEV_VERSION, read_soft_dev_version_request, NULL, NULL);
    spis_reg_install(NRF52_REG_ADDR_MODE, read_mode_request, NULL,
            write_mode_complete);
    spis_reg_install(NRF52_REG_ADDR_FW_UPGRADE_SIZE, NULL, NULL,
            write_fw_upgrade_size_complete);
    spis_reg_install(NRF52_REG_ADDR_FW_UPGRADE_CRC, NULL, NULL,
            write_fw_upgrade_crc_complete);
    spis_reg_install(NRF52_REG_ADDR_INT_STATUS, read_int_status_request, NULL, NULL);
    spis_reg_install(NRF52_REG_ADDR_INT_ENABLE, NULL, NULL,
            write_int_enable_complete);
    spis_reg_install(NRF52_REG_ADDR_ERROR_CODE, read_error_code_request, NULL, NULL);
    spis_reg_install(NRF52_REG_ADDR_DEVICE_ADDRESS, read_device_address_request, NULL, write_device_address_complete);
    spis_reg_install(NRF52_REG_ADDR_ADVERTISING_INTERVAL, read_advertising_interval_request, NULL, write_advertising_interval_complete);
    spis_reg_install(NRF52_REG_ADDR_CONNECTION_INTERVAL, read_connection_interval_request, NULL, write_connection_interval_complete);
    spis_reg_install(NRF52_REG_ADDR_PHY_MODE, read_phy_mode_request, NULL, write_phy_mode_complete);
    spis_reg_install(NRF52_REG_ADDR_TX_DATA_PORT, NULL, NULL,
            write_tx_data_port_complete);
    spis_reg_install(NRF52_REG_ADDR_TX_DATA_LENGTH, read_tx_data_length_request, NULL, NULL);
    spis_reg_install(NRF52_REG_ADDR_TX_FIFO_SIZE, read_tx_fifo_size_request, NULL, NULL);
    spis_reg_install(NRF52_REG_ADDR_RX_DATA_PORT, read_rx_data_port_request, NULL, NULL);
    spis_reg_install(NRF52_REG_ADDR_RX_DATA_LENGTH, read_rx_data_length_request, NULL, NULL);
    spis_reg_install(NRF52_REG_ADDR_RX_FIFO_SIZE, read_rx_fifo_size_request, NULL, NULL);

    /* Setup circular buffer for tx and rx data ports */
    buffer_init_policy(circular, &m_rx_data_port, (uintptr_t)m_rx_data, sizeof(m_rx_data));
    buffer_init_policy(circular, &m_tx_data_port, (uintptr_t)m_tx_data, sizeof(m_tx_data));

    /* Setup register interface abstraction over SPI slave */
    APP_ERROR_CHECK(spis_init());

    /* Setup GPIO for IRQ output */
    irq_init();

    transition_state(SM_STATE_IDLE);
}

void sm_tick(void)
{
    switch (m_state)
    {
        case SM_STATE_BOOT:
            sm_state_boot();
            break;
        case SM_STATE_IDLE:
            sm_state_idle();
            break;
        case SM_STATE_FW_UPGRADE:
            sm_state_fw_upgrade();
            break;
        case SM_STATE_GATT_SERVER:
            sm_state_gatt_server();
            break;
        default:
            break;
    }
}
