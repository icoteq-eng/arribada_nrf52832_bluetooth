/* spis_regs.c - SPI register access for NRF52x
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <string.h>

#include "app_error.h"
#include "app_util_platform.h"
#include "boards.h"
#include "nrf_drv_spis.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "sdk_config.h"
#include "spis_regs.h"

#define NRF52_REG_UNUSED 0xFF
#define NRF52_REG_TABLE_SIZE 64

#define NRF52_SPIS_DMA_ANOMALY_109_WORKAROUND_ENABLED

#define SPIS_INSTANCE 1 // SPIS instance index

typedef struct
{
    uint8_t addr;
    uint16_t (*read_request)(uint8_t *buffer, uint16_t length);
    void (*read_complete)(uint16_t length);
    void (*write_complete)(uint8_t *buffer, uint16_t length);
} nRF52_reg_t;

static const nrf_drv_spis_t spis = NRF_DRV_SPIS_INSTANCE(SPIS_INSTANCE); /**< SPIS instance. */

static volatile bool spis_rx_data_pending = false;
static volatile nrf_drv_spis_event_t last_event;

static unsigned int m_last_index = 0;
static uint8_t m_last_addr = NRF52_REG_UNUSED;
static nRF52_reg_t m_reg_table[NRF52_REG_TABLE_SIZE];

static uint8_t m_tx_buf[255];
static uint8_t m_rx_buf[255];

static int spis_transfer(void)
{
    int ret = nrf_drv_spis_buffers_set(&spis, m_tx_buf, sizeof(m_tx_buf), m_rx_buf, sizeof(m_rx_buf));

    NRF_LOG_DEBUG("spis_transfer, %d", ret);
    return ret;
}

static int lookup_reg_addr(uint8_t addr, nRF52_reg_t **reg)
{
    for (unsigned int i = 0; i < m_last_index; i++)
    {
        if ((addr & ~NRF52_SPI_WRITE_NOT_READ_ADDR) == m_reg_table[i].addr)
        {
            *reg = &m_reg_table[i];
            return NRFX_SUCCESS;
        }
    }

    return NRFX_ERROR_INVALID_PARAM;
}

static void spis_event_handler(nrf_drv_spis_event_t event)
{
    if (event.evt_type != NRF_DRV_SPIS_XFER_DONE)
        return;

    memset(m_tx_buf, 0, sizeof(m_tx_buf));

    NRF_LOG_DEBUG("m_last_addr: %02x addr: %02x event: rx: %u tx: %u", m_last_addr, m_rx_buf[0], event.rx_amount, event.tx_amount);

#ifndef NRF52_SPIS_DMA_ANOMALY_109_WORKAROUND_ENABLED
    uint8_t * m_tx_buf_shifted = &m_tx_buf[0];
#else
    // Send a 0x00 padding byte, this is because the first byte may be erroneous as stated in the DMA anomaly 109 errata
    uint8_t * m_tx_buf_shifted = &m_tx_buf[1];
#endif

    /* The last address tells us the SPI slave transaction state.
     * The MSB of the address shall be cleared if the previous
     * transaction was a read request.  Otherwise, the MSB shall
     * be set.
     */
    if ((m_last_addr & NRF52_SPI_WRITE_NOT_READ_ADDR) != 0)
    {
        nRF52_reg_t *reg;

        /* New request */
        if (lookup_reg_addr(m_rx_buf[0], &reg))
        {
            NRF_LOG_ERROR("Illegal register address: %02x", m_rx_buf[0]);
            goto done;
        }

        /* Check if this is a read or write request */
        if ((m_rx_buf[0] & NRF52_SPI_WRITE_NOT_READ_ADDR) == 0)
        {
            /* Read request */
            if (reg->read_request)
            {
                uint16_t actual_size;
                uint16_t request_size = m_rx_buf[1] |
                    (uint16_t)m_rx_buf[2] << 8;
                if (request_size > NRF52_SPI_DATA_PORT_SIZE)
                {
                    NRF_LOG_ERROR("Illegal read size: %04x", request_size);
                    request_size = NRF52_SPI_DATA_PORT_SIZE;
                }

                actual_size = reg->read_request(m_tx_buf_shifted, request_size);
                if (actual_size != request_size)
                    NRF_LOG_ERROR("Read size mismatch: actual=%04x requested=%04x", actual_size, request_size);
            }
            else
                NRF_LOG_ERROR("Register is not readable: %02x", m_rx_buf[0]);

            m_last_addr = m_rx_buf[0];
        }
        else
        {
            /* Write request */
            if (reg->write_complete)
                reg->write_complete(&m_rx_buf[1], event.rx_amount - 1);
            else
            NRF_LOG_ERROR("Register is not writeable: %02x", m_rx_buf[0]);
        }
    }
    else
    {
        /* MSB is clear, so we need to complete the last read request */
        nRF52_reg_t *reg;

        if (lookup_reg_addr(m_last_addr, &reg))
        {
            NRF_LOG_ERROR("Illegal register address: %02x", m_last_addr);
            goto done;
        }

        /* Signal completion of previous read request */
        if (reg->read_complete)
            reg->read_complete(event.rx_amount);

        /* Reset last address so we are waiting for a new request now */
        m_last_addr = NRF52_REG_UNUSED;
    }

done:
    NRF_LOG_DEBUG("xfer_complete");
    spis_transfer();
}

int spis_reg_init(void)
{
    m_last_index = 0;
    for (unsigned int i = 0; i < NRF52_REG_TABLE_SIZE; i++)
        m_reg_table[i].addr = NRF52_REG_UNUSED;

    return NRF_SUCCESS;
}

int spis_init(void)
{
    nrf_drv_spis_config_t spis_config = NRF_DRV_SPIS_DEFAULT_CONFIG;
    spis_config.csn_pin = APP_SPIS_CS_PIN;
    spis_config.miso_pin = APP_SPIS_MISO_PIN;
    spis_config.mosi_pin = APP_SPIS_MOSI_PIN;
    spis_config.sck_pin = APP_SPIS_SCK_PIN;

    int err_code = nrf_drv_spis_init(&spis, &spis_config, spis_event_handler);
    if (err_code != NRF_SUCCESS)
        return err_code;

    return spis_transfer();
}

int spis_reg_install(uint8_t addr,
                     uint16_t (*read_request)(uint8_t *buffer, uint16_t length),
                     void (*read_complete)(uint16_t length),
                     void (*write_complete)(uint8_t *buffer, uint16_t length))
{
    if (m_last_index >= NRF52_REG_TABLE_SIZE)
        return NRFX_ERROR_NO_MEM;
    
    m_reg_table[m_last_index].addr = addr;
    m_reg_table[m_last_index].read_request = read_request;
    m_reg_table[m_last_index].read_complete = read_complete;
    m_reg_table[m_last_index].write_complete = write_complete;

    m_last_index++;

    return NRFX_SUCCESS;
}

