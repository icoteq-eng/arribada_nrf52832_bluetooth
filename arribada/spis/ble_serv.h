/* ble_serv.h - Bluetooth configuration service (server side)
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _BLE_SERV_H_
#define _BLE_SERV_H_

#include <stdint.h>

void ble_init(void);
void ble_serv_start(void);
void ble_serv_stop(void);
uint32_t ble_serv_send(uint8_t *buffer, uint16_t len);
void ble_serv_kick(void);
uint16_t ble_get_max_payload_size(void);
uint16_t ble_get_soft_device_version(void);
uint16_t ble_get_connection_interval(void);
void ble_set_connection_interval(uint16_t interval);
uint8_t ble_get_preferred_phy(void);
void ble_set_preferred_phy(uint8_t mbps);
uint8_t ble_get_advertising_interval(void);
void ble_set_advertising_interval(uint16_t interval);
void ble_get_device_address(uint8_t *address, uint32_t length);
void ble_set_device_address(uint8_t *address, uint32_t length);

void ble_serv_connection_notify(bool is_connected);
void ble_serv_data_in_notify(uint8_t *data, uint8_t len);

#endif
