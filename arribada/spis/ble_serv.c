/* ble_serv.c - Bluetooth configuration service
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "amt.h"
#include "counter.h"

#include "amt.h"
#include "app_error.h"
#include "app_timer.h"
#include "ble.h"
#include "ble_advdata.h"
#include "ble_gatt.h"
#include "ble_hci.h"
#include "ble_serv.h"
#include "ble_srv_common.h"
#include "bsp_btn_ble.h"
#include "counter.h"
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "nrf_cli.h"
#include "nrf_cli_rtt.h"
#include "nrf_cli_uart.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdm.h"
#include "sdk_config.h"
#include "spis_regs.h"

#define DATA_LENGTH_DEFAULT             27                                             /**< The stack default data length. */
#define DATA_LENGTH_MAX                 251                                             /**< The stack maximum data length. */

#define CONN_INTERVAL_DEFAULT           (uint16_t)(MSEC_TO_UNITS(7.5, UNIT_1_25_MS))    /**< Default connection interval used at connection establishment by central side. */
#define CONN_INTERVAL_MIN               (uint16_t)(MSEC_TO_UNITS(7.5, UNIT_1_25_MS))    /**< Minimum acceptable connection interval, in 1.25 ms units. */
#define CONN_INTERVAL_MAX               (uint16_t)(MSEC_TO_UNITS(500, UNIT_1_25_MS))    /**< Maximum acceptable connection interval, in 1.25 ms units. */

#define ADV_INTERVAL_DEFAULT            (uint16_t)(MSEC_TO_UNITS(20.0,   UNIT_0_625_MS)) /**< Default connection interval used at connection establishment by central side. */
#define ADV_INTERVAL_MIN                (uint16_t)(MSEC_TO_UNITS(20.0,   UNIT_0_625_MS)) /**< Minimum acceptable advertising interval, in 0.625 ms units. */
#define ADV_INTERVAL_MAX                (uint16_t)(MSEC_TO_UNITS(4000.0, UNIT_0_625_MS)) /**< Maximum acceptable advertising interval, in 0.625 ms units. */

#define CONN_SUP_TIMEOUT                (uint16_t)(MSEC_TO_UNITS(4000,  UNIT_10_MS))    /**< Connection supervisory timeout (4 seconds). */
#define SLAVE_LATENCY                   0                                               /**< Slave latency. */

#define APP_BLE_CONN_CFG_TAG            1                                               /**< A tag that refers to the BLE stack configuration. */
#define APP_BLE_OBSERVER_PRIO           3                                               /**< Application's BLE observer priority. You shouldn't need to modify this value. */

static uint8_t m_adv_handle = BLE_GAP_ADV_SET_HANDLE_NOT_SET;                           /**< Advertising handle used to identify an advertising set. */
static uint8_t m_enc_advdata[BLE_GAP_ADV_SET_DATA_SIZE_MAX];                            /**< Buffer for storing an encoded advertising set. */

static volatile bool currently_advertising = false;                                     /**< Are we currently advertising? */

static volatile ble_gap_addr_t ble_addr;
static ble_gap_addr_t ble_addr_non_volatile;

/**@brief Struct that contains pointers to the encoded advertising data. */
static ble_gap_adv_data_t m_adv_data =
{
    .adv_data =
    {
        .p_data = m_enc_advdata,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX
    },
    .scan_rsp_data =
    {
        .p_data = NULL,
        .len    = 0

    }
};

typedef struct
{
    uint16_t        att_mtu;                    /**< GATT ATT MTU, in bytes. */
    uint16_t        advertising_interval;       /**< Advertising interval expressed in units of 0.625 ms. */
    uint16_t        conn_interval;              /**< Connection interval expressed in units of 1.25 ms. */
    ble_gap_phys_t  phys;                       /**< Preferred PHYs. */
    uint8_t         data_len;                   /**< Data length. */
    bool            conn_evt_len_ext_enabled;   /**< Connection event length extension status. */
} test_params_t;


NRF_BLE_GATT_DEF(m_gatt);                       /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                         /**< Context for the Queued Write module.*/
BLE_DB_DISCOVERY_DEF(m_ble_db_discovery);       /**< DB discovery module instance. */

static nrf_ble_amts_t     m_amts;
NRF_SDH_BLE_OBSERVER(m_amts_ble_obs, BLE_AMTS_BLE_OBSERVER_PRIO, nrf_ble_amts_on_ble_evt, &m_amts);

NRF_CLI_UART_DEF(cli_uart, 0, 64, 16);
NRF_CLI_RTT_DEF(cli_rtt);
NRF_CLI_DEF(m_cli_uart, "throughput example:~$ ", &cli_uart.transport, '\r', 4);
NRF_CLI_DEF(m_cli_rtt,  "throughput example:~$ ", &cli_rtt.transport,  '\n', 4);

static bool volatile m_notif_enabled;
static bool volatile m_mtu_exchanged;
static bool volatile m_data_length_updated;
static bool volatile m_phy_updated;
static bool volatile m_conn_interval_configured;

static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current BLE connection .*/
static uint8_t m_gap_role     = BLE_GAP_ROLE_INVALID;       /**< BLE role for this connection, see @ref BLE_GAP_ROLES */

// Name to use for advertising and connection.
//static char const m_target_periph_name[] = DEVICE_NAME;

// Test parameters.
// Settings like ATT MTU size are set only once on the dummy board.
// Make sure that defaults are sensible.
static test_params_t m_test_params =
{
    .att_mtu                  = NRF_SDH_BLE_GATT_MAX_MTU_SIZE,
    .data_len                 = NRF_SDH_BLE_GAP_DATA_LENGTH,
    .advertising_interval     = ADV_INTERVAL_DEFAULT,
    .conn_interval            = CONN_INTERVAL_DEFAULT,
    .conn_evt_len_ext_enabled = true,
    .phys.tx_phys             = BLE_GAP_PHY_1MBPS,
    .phys.rx_phys             = BLE_GAP_PHY_1MBPS,
};

// Connection parameters requested for connection.
static ble_gap_conn_params_t m_conn_param =
{
    .min_conn_interval = CONN_INTERVAL_MIN,   // Minimum connection interval.
    .max_conn_interval = CONN_INTERVAL_MAX,   // Maximum connection interval.
    .slave_latency     = SLAVE_LATENCY,       // Slave latency.
    .conn_sup_timeout  = CONN_SUP_TIMEOUT     // Supervisory timeout.
};

char const * phy_str(ble_gap_phys_t phys)
{
    static char const * str[] =
    {
        "1 Mbps",
        "2 Mbps",
        "Coded",
        "Unknown"
    };

    switch (phys.tx_phys)
    {
        case BLE_GAP_PHY_1MBPS:
            return str[0];

        case BLE_GAP_PHY_2MBPS:
        case BLE_GAP_PHY_2MBPS | BLE_GAP_PHY_1MBPS:
        case BLE_GAP_PHY_2MBPS | BLE_GAP_PHY_1MBPS | BLE_GAP_PHY_CODED:
            return str[1];

        case BLE_GAP_PHY_CODED:
            return str[2];

        default:
            return str[3];
    }
}

/**@brief Function for starting advertising. */
static void advertising_start(void)
{
    if (!currently_advertising)
    {
        NRF_LOG_INFO("Starting advertising.");
    
        ret_code_t err_code = sd_ble_gap_adv_start(m_adv_handle, APP_BLE_CONN_CFG_TAG);
    
        if (NRF_SUCCESS == err_code)
            currently_advertising = true;
    
        APP_ERROR_CHECK(err_code);
    }
}

/**@brief Function for stopping advertising. */
static void advertising_stop(void)
{
    if (currently_advertising)
    {
        NRF_LOG_INFO("Stopping advertising.");

        ret_code_t err_code = sd_ble_gap_adv_stop(m_adv_handle);
        currently_advertising = false;

        APP_ERROR_CHECK(err_code);
    }
}

/**@brief Function for handling BLE_GAP_EVT_CONNECTED events.
 * Save the connection handle and GAP role, then discover the peer DB.
 */
static void on_ble_gap_evt_connected(ble_gap_evt_t const * p_gap_evt)
{
    ret_code_t err_code;

    m_conn_handle = p_gap_evt->conn_handle;
    m_gap_role    = p_gap_evt->params.connected.role;

    if (m_gap_role == BLE_GAP_ROLE_PERIPH)
    {
        NRF_LOG_DEBUG("Connected as a peripheral.");
    }
    else if (m_gap_role == BLE_GAP_ROLE_CENTRAL)
    {
        NRF_LOG_DEBUG("Connected as a central.");
    }

    // Stop advertising.
    (void) sd_ble_gap_adv_stop(m_adv_handle);

    // Assign connection handle to the Queued Write module.
    err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
    APP_ERROR_CHECK(err_code);

    if (m_gap_role == BLE_GAP_ROLE_PERIPH)
    {
        NRF_LOG_INFO("Sending PHY Update, %s.", phy_str(m_test_params.phys));

        err_code = sd_ble_gap_phy_update(p_gap_evt->conn_handle, &m_test_params.phys);
        APP_ERROR_CHECK(err_code);
    }

    ble_serv_connection_notify(true);
}

/**@brief Function for handling BLE_GAP_EVT_DISCONNECTED events.
 * Unset the connection handle and terminate the test.
 */
static void on_ble_gap_evt_disconnected(ble_gap_evt_t const * p_gap_evt)
{
    m_conn_handle = BLE_CONN_HANDLE_INVALID;

    NRF_LOG_INFO("Disconnected: reason 0x%x.", p_gap_evt->params.disconnected.reason);

    m_notif_enabled            = false;
    m_mtu_exchanged            = false;
    m_data_length_updated      = false;
    m_phy_updated              = false;
    m_conn_interval_configured = false;

    // If we didn't terminate the connection ourselves
    if (BLE_HCI_LOCAL_HOST_TERMINATED_CONNECTION != p_gap_evt->params.disconnected.reason)
        advertising_start(); // Then start advertising again

    ble_serv_connection_notify(false);
}

/**@brief Function for handling BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t              err_code;
    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_ADV_REPORT:
            //on_adv_report(&p_gap_evt->params.adv_report);
            break;

        case BLE_GAP_EVT_CONNECTED:
            currently_advertising = false;
            on_ble_gap_evt_connected(p_gap_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_ble_gap_evt_disconnected(p_gap_evt);
            break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE:
        {
            m_conn_interval_configured = true;
            NRF_LOG_INFO("Connection interval updated: 0x%x, 0x%x.",
                p_gap_evt->params.conn_param_update.conn_params.min_conn_interval,
                p_gap_evt->params.conn_param_update.conn_params.max_conn_interval);
        } break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
        {
            // Accept parameters requested by the peer.
            ble_gap_conn_params_t params;
            params = p_gap_evt->params.conn_param_update_request.conn_params;
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle, &params);
            APP_ERROR_CHECK(err_code);

            NRF_LOG_INFO("Connection interval updated (upon request): 0x%x, 0x%x.",
                p_gap_evt->params.conn_param_update_request.conn_params.min_conn_interval,
                p_gap_evt->params.conn_param_update_request.conn_params.max_conn_interval);
        } break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
        {
            err_code = sd_ble_gatts_sys_attr_set(p_gap_evt->conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT: // Fallthrough.
        case BLE_GATTS_EVT_TIMEOUT:
        {
            NRF_LOG_INFO("GATT timeout, disconnecting.");
            err_code = sd_ble_gap_disconnect(m_conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_PHY_UPDATE:
        {
            ble_gap_evt_phy_update_t const * p_phy_evt = &p_ble_evt->evt.gap_evt.params.phy_update;

            if (p_phy_evt->status == BLE_HCI_STATUS_CODE_LMP_ERROR_TRANSACTION_COLLISION)
            {
                // Ignore LL collisions.
                NRF_LOG_INFO("LL transaction collision during PHY update.");
                break;
            }

            m_phy_updated = true;

            ble_gap_phys_t phys = {0};
            phys.tx_phys = p_phy_evt->tx_phy;
            phys.rx_phys = p_phy_evt->rx_phy;
            NRF_LOG_INFO("PHY update %s. PHY set to %s.",
                         (p_phy_evt->status == BLE_HCI_STATUS_CODE_SUCCESS) ?
                         "accepted" : "rejected",
                         phy_str(phys));
        } break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            err_code = sd_ble_gap_phy_update(p_gap_evt->conn_handle, &m_test_params.phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_TIMEOUT: // This will occur if an advertising timeout is set
            currently_advertising = false;

        default:
            // No implementation needed.
            break;
    }
}

/**@brief AMT server event handler. */
static void amts_evt_handler(nrf_ble_amts_evt_t evt)
{
    ret_code_t err_code;

    switch (evt.evt_type)
    {
        case NRF_BLE_AMTS_EVT_NOTIF_ENABLED:
        {
            NRF_LOG_INFO("Notifications enabled.");

            m_notif_enabled = true;

            if (m_test_params.conn_interval != CONN_INTERVAL_DEFAULT)
            {
                NRF_LOG_INFO("Updating connection parameters..");
                m_conn_param.min_conn_interval = m_test_params.conn_interval;
                m_conn_param.max_conn_interval = m_test_params.conn_interval;
                err_code = sd_ble_gap_conn_param_update(m_conn_handle, &m_conn_param);

                if (err_code != NRF_SUCCESS)
                {
                    NRF_LOG_ERROR("sd_ble_gap_conn_param_update() failed: 0x%x.", err_code);
                }
            }
            else
            {
                m_conn_interval_configured = true;
            }
        } break;

        case NRF_BLE_AMTS_EVT_NOTIF_DISABLED:
        {
            NRF_LOG_INFO("Notifications disabled.");
        } break;
    }
}

/**@brief Function for handling events from the GATT library. */
static void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    switch (p_evt->evt_id)
    {
        case NRF_BLE_GATT_EVT_ATT_MTU_UPDATED:
        {
            m_mtu_exchanged = true;
            NRF_LOG_INFO("ATT MTU exchange completed. MTU set to %u bytes.",
                         p_evt->params.att_mtu_effective);
        } break;

        case NRF_BLE_GATT_EVT_DATA_LENGTH_UPDATED:
        {
            m_data_length_updated = true;
            NRF_LOG_INFO("Data length updated to %u bytes.", p_evt->params.data_length);
        } break;
    }

    nrf_ble_amts_on_gatt_evt(&m_amts, p_evt);
}

/**@brief Function for setting up advertising data. */
static void advertising_data_set(void)
{

    ret_code_t ret;

    ble_gap_adv_params_t adv_params =
    {
        .properties    =
        {
            .type = BLE_GAP_ADV_TYPE_CONNECTABLE_SCANNABLE_UNDIRECTED,
        },
        .p_peer_addr   = NULL,
        .filter_policy = BLE_GAP_ADV_FP_ANY,
        .interval      = m_test_params.advertising_interval,
        .duration      = 0,

        .primary_phy   = BLE_GAP_PHY_1MBPS, // Must be changed to connect in long range. (BLE_GAP_PHY_CODED)
        .secondary_phy = BLE_GAP_PHY_1MBPS,
    };

    ble_advdata_t const adv_data =
    {
        .name_type          = BLE_ADVDATA_FULL_NAME,
        .flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE,
        .include_appearance = false,
    };

    ret = ble_advdata_encode(&adv_data, m_adv_data.adv_data.p_data, &m_adv_data.adv_data.len);
    APP_ERROR_CHECK(ret);

    ret = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data, &adv_params);
    APP_ERROR_CHECK(ret);
}

/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Queued Write module.
 */
static void qwr_init(void)
{
    ret_code_t         err_code;
    nrf_ble_qwr_init_t qwr_init_obj = {0};

    // Initialize Queued Write Module.
    qwr_init_obj.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init_obj);
    APP_ERROR_CHECK(err_code);
}

static void server_init(void)
{
    qwr_init();
    nrf_ble_amts_init(&m_amts, amts_evt_handler);
}

/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}

/**@brief Function for initializing GAP parameters.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (uint8_t const *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_ppcp_set(&m_conn_param);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the GATT library. */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);
}

void gatt_mtu_set(uint16_t att_mtu)
{
    ret_code_t err_code;

    m_test_params.att_mtu = att_mtu;

    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, att_mtu);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_central_set(&m_gatt, att_mtu);
    APP_ERROR_CHECK(err_code);
}

void conn_evt_len_ext_set(bool status)
{
    ret_code_t err_code;
    ble_opt_t  opt;

    memset(&opt, 0x00, sizeof(opt));
    opt.common_opt.conn_evt_ext.enable = status ? 1 : 0;

    err_code = sd_ble_opt_set(BLE_COMMON_OPT_CONN_EVT_EXT, &opt);
    APP_ERROR_CHECK(err_code);

    m_test_params.conn_evt_len_ext_enabled = status;
}

void data_len_set(uint8_t value)
{
    ret_code_t err_code;
    err_code = nrf_ble_gatt_data_length_set(&m_gatt, BLE_CONN_HANDLE_INVALID, value);
    APP_ERROR_CHECK(err_code);

    m_test_params.data_len = value;
}

void ble_init(void)
{
    NRF_LOG_INFO("ble_stack_init");
    ble_stack_init();
    NRF_LOG_INFO("gap_params_init");
    gap_params_init();
    NRF_LOG_INFO("gatt_init");
    gatt_init();
    NRF_LOG_INFO("advertising_data_set");
    advertising_data_set();

    NRF_LOG_INFO("server_init");
    server_init();

    // Get the random MAC address that we start with
    sd_ble_gap_addr_get(&ble_addr_non_volatile);
    ble_addr = ble_addr_non_volatile;
}

void ble_serv_start(void)
{
    gatt_mtu_set(m_test_params.att_mtu);
    conn_evt_len_ext_set(m_test_params.conn_evt_len_ext_enabled);

    data_len_set(251);

    // Set device address if we have a custom one
    ble_addr_non_volatile = ble_addr;
    sd_ble_gap_addr_set(&ble_addr_non_volatile);

    advertising_start();
}

void ble_serv_stop()
{
    // Stop advertising if we were
    advertising_stop();

    // Disconnect if we were connected
    if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        NRF_LOG_INFO("Disconnecting...");

        ret_code_t err_code;
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);

        if (err_code != NRF_SUCCESS)
        {
            NRF_LOG_ERROR("sd_ble_gap_disconnect() failed: 0x%0x.", err_code);
        }
    }
}

inline uint32_t ble_serv_send(uint8_t * buffer, uint16_t len)
{
    if (m_notif_enabled) // Only allow sending if notifications on the server have been enabled
        return nrf_ble_amts_send(&m_amts, buffer, len);
    return NRF_ERROR_FORBIDDEN;
}

__attribute__((weak)) void ble_serv_connection_notify(bool is_connected)
{
    NRF_LOG_INFO("%s() NOT IMPLEMENTED", __FUNCTION__);
}

__attribute__((weak)) void ble_serv_data_in_notify(uint8_t *data, uint8_t len)
{
    NRF_LOG_INFO("%s() NOT IMPLEMENTED", __FUNCTION__);
}

uint16_t ble_get_max_payload_size(void)
{
    return m_amts.max_payload_len;
}

uint16_t ble_get_soft_device_version(void)
{
    //ble_version_t ble_version;

    //sd_ble_version_get(&ble_version);

    uint16_t * ble_version = (uint16_t *) 0x300C;
    return *ble_version;
    //return ble_version.subversion_number;
}

uint16_t ble_get_connection_interval(void)
{
    return m_test_params.conn_interval;
}

void ble_set_connection_interval(uint16_t interval)
{
    // Don't permit invalid connection intervals
    if (interval < CONN_INTERVAL_MIN ||
        interval > CONN_INTERVAL_MAX)
        return;

    m_test_params.conn_interval = interval;
}

uint8_t ble_get_preferred_phy(void)
{
    return m_test_params.phys.tx_phys;
}

void ble_set_preferred_phy(uint8_t mbps)
{
    // Don't permit invalid phy speeds
    if (mbps != BLE_GAP_PHY_1MBPS &&
        mbps != BLE_GAP_PHY_2MBPS)
        return;

    ble_gap_phys_t p_phy;
    p_phy.tx_phys = mbps;
    p_phy.rx_phys = mbps;
    memcpy(&m_test_params.phys, &p_phy, sizeof(ble_gap_phys_t));
}

uint8_t ble_get_advertising_interval(void)
{
    return m_test_params.advertising_interval;
}

void ble_set_advertising_interval(uint16_t interval)
{
    // Don't permit invalid advertising intervals
    if (interval < ADV_INTERVAL_MIN ||
        interval > ADV_INTERVAL_MAX)
        return;

    // If our advertising interval has changed
    if (m_test_params.advertising_interval != interval)
    {
        m_test_params.advertising_interval = interval;

        // We have to stop the advertising service, reconfigure it and then start it again
        bool were_advertising = currently_advertising;
        
        advertising_stop();
        advertising_data_set();

        // Start advertising again if we were previously
        if (were_advertising)
            advertising_start();
    }
}

void ble_get_device_address(uint8_t *address, uint32_t length)
{
    // Check address buffer is big enough
    if (length != NRF52_REG_SIZE_DEVICE_ADDRESS)
        return;

    for (uint32_t i = 0; i < length; ++i)
        address[i] = ble_addr.addr[i];
}

void ble_set_device_address(uint8_t *address, uint32_t length)
{
    // Check length is correct
    if (length != NRF52_REG_SIZE_DEVICE_ADDRESS)
        return;

    for (uint32_t i = 0; i < length; ++i)
        ble_addr.addr[i] = address[i];

    ble_addr_non_volatile.addr_type = BLE_GAP_ADDR_TYPE_RANDOM_STATIC;
    ble_addr.addr[5] |= ((1 << 7) | (1 << 6)); // The top 2 bits must be set in Private address mode
}

/**
 * @}
 */