/* firmware.c - Firmware writing abstraction layer
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <nrfx.h>
#include "nrf_drv_spis.h"
#include "nrf_sdh.h"
#include "nrf_spis.h"
#include "firmware.h"

#define SPIS_INSTANCE 1 // SPIS instance index
#define NRF52_PAGE_SIZE (4096)

static uint32_t m_writing_address_priv; // The FLASH address we're currently writing to
static uint32_t m_bytes_remaining_priv; // Number of buffer.bytes yet to be written to FLASH
static uint32_t m_total_bytes_to_write; // Total number of bytes to write to flash
static bool m_buffers_set; // Have the SPIS easyDMA buffers been set?

static uint8_t m_tx_buf[255];
static uint8_t m_rx_buf[255];

static const nrf_drv_spis_t m_spis = NRF_DRV_SPIS_INSTANCE(SPIS_INSTANCE);

static union
{
    uint32_t word;
    uint8_t bytes[4];
} buffer_priv;

__RAMFUNC void firmware_write_word_priv(uint32_t address, uint32_t value)
{
    // Enable write
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen;
    __ISB();
    __DSB();

    *(uint32_t *)address = value;
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy) // Wait for flash ready (Takes 41us)
    {}

    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;
    __ISB();
    __DSB();
}

__RAMFUNC void firmware_page_erase_priv(uint32_t address)
{
    // Enable erase
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Een;
    __ISB();
    __DSB();

    // Erase the page
    NRF_NVMC->ERASEPAGE = address;
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy) // Wait for flash ready (Takes 85ms)
    {}

    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;
    __ISB();
    __DSB();
}

__RAMFUNC int firmware_prepare(uint32_t size)
{
    m_bytes_remaining_priv = 0;

    m_writing_address_priv = 0x0; // Start the firmware update from the start of the FLASH region

    nrf_sdh_disable_request(); // Stop the bluetooth stack/softdevice
    __disable_irq(); // Prevent us from being interrupted by anything

    // Erase every page required. This takes 85ms * numberOfPages
    uint32_t number_of_pages_to_erase = (size / NRF52_PAGE_SIZE) + (size % NRF52_PAGE_SIZE != 0); // Integer round up ceil()
    for (uint32_t i = 0; i < number_of_pages_to_erase; ++i)
    {
        uint32_t first_word_of_page;

        // Get the address of the start of each page
        first_word_of_page = m_writing_address_priv + i * NRF52_PAGE_SIZE;
        first_word_of_page -= first_word_of_page % NRF52_PAGE_SIZE;

        firmware_page_erase_priv(first_word_of_page);
    }

    m_buffers_set = false;
    m_total_bytes_to_write = size;

    nrf_spis_task_trigger(m_spis.p_reg, NRF_SPIS_TASK_ACQUIRE); // Request the SPIS semaphore

    return FIRMWARE_NO_ERROR;
}

__RAMFUNC int firmware_write(void)
{
    uint32_t total_bytes_written = 0;

    while (total_bytes_written < m_total_bytes_to_write)
    {
        // As all IRQs are disabled we must manually poll the SPIS interrupt flags

        // Check for SPIS semaphore acquired event
        if (nrf_spis_event_check(m_spis.p_reg, NRF_SPIS_EVENT_ACQUIRED))
        {
            nrf_spis_event_clear(m_spis.p_reg, NRF_SPIS_EVENT_ACQUIRED);

            if (!m_buffers_set)
            {
                nrf_spis_tx_buffer_set(m_spis.p_reg, m_tx_buf, sizeof(m_tx_buf));
                nrf_spis_rx_buffer_set(m_spis.p_reg, m_rx_buf, sizeof(m_rx_buf));

                nrf_spis_task_trigger(m_spis.p_reg, NRF_SPIS_TASK_RELEASE); // Release the SPIS semaphore

                m_buffers_set = true;
            }
        }

        // Check for SPI transaction complete event
        if (nrf_spis_event_check(m_spis.p_reg, NRF_SPIS_EVENT_END))
        {
            nrf_spis_event_clear(m_spis.p_reg, NRF_SPIS_EVENT_END);

            nrf_spis_task_trigger(m_spis.p_reg, NRF_SPIS_TASK_ACQUIRE); // Request the SPIS semaphore

            uint32_t rx_received = nrf_spis_rx_amount_get(m_spis.p_reg);

            // Iterate through every discrete byte
            for (uint32_t i = 0; i < rx_received; ++i)
            {
                // Fill up blocks of 32 bits
                buffer_priv.bytes[m_bytes_remaining_priv] = m_rx_buf[i];
                m_bytes_remaining_priv++;

                // If we have a full 32 bits, write it to FLASH
                if (m_bytes_remaining_priv == 4)
                {
                    firmware_write_word_priv(m_writing_address_priv, buffer_priv.word);
                    m_writing_address_priv += 4;
                    m_bytes_remaining_priv = 0;
                }
            }

            total_bytes_written += rx_received;

            m_buffers_set = false;
        }

    }

    // Flush any remaining data to FLASH
    if (m_bytes_remaining_priv)
    {
        firmware_write_word_priv(m_writing_address_priv, buffer_priv.word);
        m_writing_address_priv += 4;
        m_bytes_remaining_priv = 0;
    }

    return FIRMWARE_NO_ERROR;
}

__RAMFUNC int firmware_finalise(void)
{
    // Reset the device

    // Below is a copy of NVIC_SystemReset()
    // NVIC_SystemReset() can't be used itself as it does not inline properly and remains in FLASH

    __DSB();
    SCB->AIRCR  = (uint32_t)( (0x5FAUL << SCB_AIRCR_VECTKEY_Pos)    |
                              (SCB->AIRCR & SCB_AIRCR_PRIGROUP_Msk) |
                              SCB_AIRCR_SYSRESETREQ_Msk );
    __DSB();

    for (;;)
    {
        __NOP(); // Wait until reset
    }

    return FIRMWARE_NO_ERROR;
}