/* firmware.h - Firmware writing abstraction layer
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _FIRMWARE_H_
#define _FIRMWARE_H_

#include <stdint.h>

#define FIRMWARE_NO_ERROR      ( 0)
#define FIRMWARE_INVALID_TYPE  (-1)

// Used for placing function into RAM
// Enforce long calls so that we can jump directly from FLASH to RAM
// Optimise for size to save RAM space
#define __RAMFUNC __attribute__ ((long_call, optimize("Os"), section (".ram_func")))

typedef enum
{
    FIRMWARE_APP,
    FIRMWARE_SOFTDEVICE
} firmware_t;

int firmware_prepare(uint32_t size);
int firmware_write(void);
int firmware_finalise(void);

#endif /* _FIRMWARE_H_ */
