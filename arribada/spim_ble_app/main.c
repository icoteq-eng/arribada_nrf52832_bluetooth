/* main.c - BLE SPI client configuration test program
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "nrf_drv_spi.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "boards.h"
#include "app_error.h"
#include <string.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_pwr_mgmt.h"
#include "app_timer.h"

#include "nRF52x_regs.h"
#include "ble_cli.h"

#define SPI_INSTANCE  0 /**< SPI instance index. */
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE); /**< SPI instance. */
static volatile bool spi_xfer_done; /**< Flag used to indicate that SPI instance completed the transfer. */

static volatile bool m_connected = false;
static volatile bool m_tx_busy = false;
static volatile unsigned int m_packets_sent = 0;
static volatile unsigned int m_packets_received = 0;

static uint8_t m_tx_buf[256];
static uint8_t m_rx_buf[256];
static uint8_t m_client_uuid[16] =
{
    0x23, 0xD1, 0xBC, 0xEA, 0x5F, 0x78, 0x23, 0x15,
    0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00
};
static uint8_t m_server_uuid[16] =
{
    0x23, 0xD1, 0xBC, 0xEA, 0x5F, 0x78, 0x23, 0x15,
    0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00
};

static void ble_cli_config_out_handler(uint8_t len, uint8_t *data)
{
    NRF_LOG_DEBUG("ble_cli_config_out_handler: len=%u", len);
    m_packets_received++;
}

static void ble_cli_config_in_handler(void)
{
    NRF_LOG_DEBUG("ble_cli_config_in_handler");
    m_packets_sent++;
    m_tx_busy = false;
}

static void ble_cli_connection_handler(bool is_connected)
{
    NRF_LOG_INFO("ble_cli_connection_handler: %u", is_connected);
    m_connected = is_connected;
}

/**
 * @brief SPI user event handler.
 * @param event
 */
static void spi_event_handler(nrf_drv_spi_evt_t const * p_event, void * p_context)
{
    spi_xfer_done = true;
}

static inline void wait_spi_complete(void)
{
    while (!spi_xfer_done)
        nrf_pwr_mgmt_run();
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module.
 */
static void timers_init(void)
{
    // Initialize timer module, making it use the scheduler
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

static void wait_connection(void)
{
    while (!m_connected)
    {
        NRF_LOG_FLUSH();
        nrf_pwr_mgmt_run();
    }
}

static void write_register(uint8_t addr, uint8_t *buffer, uint16_t size)
{
    m_tx_buf[0] = addr | 0x80;
    spi_xfer_done = false;
    NRF_LOG_DEBUG("write: %02x size: %04x", addr, size);
    memcpy(&m_tx_buf[1], buffer, size);
    nrf_drv_spi_transfer(&spi, m_tx_buf, size + 1, m_rx_buf, 0);
    wait_spi_complete();
}

static void read_register(uint8_t addr, uint8_t *buffer, uint16_t size)
{
    m_tx_buf[0] = addr;
    m_tx_buf[1] = size & 0xFF;
    m_tx_buf[2] = (size >> 8) & 0xFF;
    spi_xfer_done = false;

    NRF_LOG_DEBUG("read: %02x size: %04x", addr, size);

    nrf_drv_spi_transfer(&spi, m_tx_buf, 3, m_rx_buf, 0);
    wait_spi_complete();
    nrf_delay_us(100);
    spi_xfer_done = false;
    nrf_drv_spi_transfer(&spi, m_tx_buf, size, m_rx_buf, size);
    wait_spi_complete();
    memcpy(buffer, m_rx_buf, size);
}

int main(void)
{
    // Enable the constant latency sub power mode to minimize the time it takes
    // for the SPIS peripheral to become active after the CSN line is asserted
    // (when the CPU is in sleep mode).
    //NRF_POWER->TASKS_CONSTLAT = 1;

    bsp_board_init(BSP_INIT_LEDS);
    timers_init();

    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_DEFAULT_BACKENDS_INIT();

    APP_ERROR_CHECK(nrf_pwr_mgmt_init());

    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.ss_pin = SPI_SS_PIN;
    spi_config.miso_pin = SPI_MISO_PIN;
    spi_config.mosi_pin = SPI_MOSI_PIN;
    spi_config.sck_pin = SPI_SCK_PIN;
    spi_config.frequency = NRF_SPI_FREQ_4M;
    APP_ERROR_CHECK(
        nrf_drv_spi_init(&spi, &spi_config, spi_event_handler, NULL));

    NRF_LOG_INFO("SPIM BLE test app");
    NRF_LOG_FLUSH();

    uint16_t vers;
    read_register(NRF52_REG_ADDR_APP_VERSION, (uint8_t *) &vers, sizeof(vers));
    NRF_LOG_INFO("APP_VERSION=%04x", vers);
    NRF_LOG_FLUSH();
    nrf_delay_ms(10);

    uint8_t mode;

#if 0
    mode = NRF52_MODE_LOOPBACK;

    /* Put the slave into loopback test mode */
    write_register(NRF52_REG_ADDR_MODE, &mode, 1);
    NRF_LOG_FLUSH();
    nrf_delay_ms(5);

    while (true)
    {
        NRF_LOG_INFO("Tick");
        write_register(NRF52_REG_ADDR_TX_DATA_PORT, m_rx_buf, 254);
        NRF_LOG_FLUSH();
        nrf_delay_ms(5);
        read_register(NRF52_REG_ADDR_RX_DATA_PORT, m_rx_buf, 254);
        bsp_board_led_invert(BSP_BOARD_LED_0);
        NRF_LOG_FLUSH();
        nrf_delay_us(5);
    }
#endif

    NRF_LOG_INFO("Configuring SPIS bluetooth...");

    /* Set the server UUID */
    write_register(NRF52_REG_ADDR_OWN_UUID, m_server_uuid, 16);
    nrf_delay_ms(10);

    NRF_LOG_INFO("Configuring SPIS GATT server mode...");

    /* Enable the server BLE function via SPI */
    mode = NRF52_MODE_GATT_SERVER;
    write_register(NRF52_REG_ADDR_MODE, &mode, 1);
    NRF_LOG_FLUSH();
    nrf_delay_ms(10);

    /* Start BLE client */
    ble_uuid128_t cli_uuid;
    memcpy(cli_uuid.uuid128, m_client_uuid, sizeof(m_client_uuid));

    ble_cli_start(cli_uuid,
        ble_cli_config_out_handler,
        ble_cli_config_in_handler,
        ble_cli_connection_handler);

    NRF_LOG_INFO("SPIM BLE waiting for connection...");

    wait_connection();

    NRF_LOG_INFO("BLE Connected");
    NRF_LOG_FLUSH();

    nrf_delay_ms(500);

    NRF_LOG_INFO("Slave -> Master...");
    NRF_LOG_FLUSH();

    m_packets_received = 0;

    app_timer_resume();
    uint32_t start = app_timer_cnt_get();

    while (m_packets_received < 1000 && m_connected)
    {
        /* Keep FIFO topped up */
        write_register(NRF52_REG_ADDR_TX_DATA_PORT, m_tx_buf, 244);
    }

    NRF_LOG_INFO("End of test: %u packets in %u ticks", m_packets_received, (app_timer_cnt_get() - start));
    NRF_LOG_FLUSH();

    NRF_LOG_INFO("Master -> Slave...");
    NRF_LOG_FLUSH();

    m_packets_sent = 0;

    start = app_timer_cnt_get();

    while (m_packets_sent < 1000 && m_connected)
    {
        uint32_t err;
        do
        {
            err = ble_cli_send_config_in(244, m_tx_buf);
        } while (err == NRF_SUCCESS);
        nrf_pwr_mgmt_run();
    }

    NRF_LOG_INFO("End of test: %u packets in %u ticks", m_packets_sent, (app_timer_cnt_get() - start));
    NRF_LOG_FLUSH();
}
