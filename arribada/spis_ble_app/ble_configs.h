/**
 * Copyright (c) 2015 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup ble_configs Arribada Configuration Service Server
 * @{
 * @ingroup ble_sdk_srv
 *
 * @brief Arribada Configuration Service Server module.
 *
 * @details This module implements a custom configuration with an configuration input and
 *          configuration output characteristic.
 *          During initialization, the module adds the configuration characteristics
 *          to the BLE stack database.
 *
 *          The application must supply an event handler for receiving configuration
 *          data. Using this handler, the service notifies the application when the
 *          configuration data has been received.
 *
 *          The service also provides a function for letting the application notify
 *          configuration to be sent.
 *
 * @note    The application must register this module as BLE event observer using the
 *          NRF_SDH_BLE_OBSERVER macro. Example:
 *          @code
 *              ble_hids_t instance;
 *              NRF_SDH_BLE_OBSERVER(anything, BLE_HIDS_BLE_OBSERVER_PRIO,
 *                                   ble_hids_on_ble_evt, &instance);
 *          @endcode
 */

#ifndef BLE_CONFIGS_H__
#define BLE_CONFIGS_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "nrf_sdh_ble.h"

#ifdef __cplusplus
extern "C" {
#endif

/**@brief   Macro for defining a ble_configs instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define BLE_CONFIGS_DEF(_name)                                                                          \
static ble_configs_t _name;                                                                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_CONFIGS_BLE_OBSERVER_PRIO,                                                     \
                     ble_configs_on_ble_evt, &_name)

#define CONFIGS_UUID_SERVICE          0x1523
#define CONFIGS_UUID_CONFIG_OUT_CHAR  0x1524
#define CONFIGS_UUID_CONFIG_IN_CHAR   0x1525

// Forward declaration of the ble_configs_t type.
typedef struct ble_configs_s ble_configs_t;

typedef void (*ble_configs_in_handler_t) (uint16_t conn_handle, ble_configs_t * p_configs, uint8_t len, uint8_t *data);
typedef void (*ble_configs_out_handler_t) (uint16_t conn_handle);

/** @brief Configuration Service init structure. This structure contains all options and data needed for
 *         initialization of the service.
 */
typedef struct
{
    ble_uuid128_t  base_uuid;
    ble_configs_in_handler_t    config_in_handler; /**< Event handler to be called when configuration in is written. */
    ble_configs_out_handler_t   config_out_handler; /**< Event handler to be called when configuration out is complete. */
} ble_configs_init_t;

/**@brief LED Button Service structure. This structure contains various status information for the service. */
struct ble_configs_s
{
    uint16_t                    service_handle;      /**< Handle of LED Button Service (as provided by the BLE stack). */
    ble_gatts_char_handles_t    config_in_handles;    /**< Handles related to input configuration */
    ble_gatts_char_handles_t    config_out_handles; /**< Handles related to output configuration */
    uint8_t                     uuid_type;           /**< UUID type for the configuration service */
    ble_configs_in_handler_t    config_in_handler;   /**< Event handler to be called when configuration input has been written */
    ble_configs_out_handler_t   config_out_handler; /**< Event handler to be called when configuration out is complete. */
};

/**@brief Function for initializing the Configuration Service.
 *
 * @param[out] p_configs  Configuration Service structure. This structure must be supplied by
 *                        the application. It is initialized by this function and will later
 *                        be used to identify this particular service instance.
 * @param[in] p_configs_init  Information needed to initialize the service.
 *
 * @retval NRF_SUCCESS If the service was initialized successfully. Otherwise, an error code is returned.
 */
uint32_t ble_configs_init(ble_configs_t * p_configs, const ble_configs_init_t * p_configs_init);

/**@brief Function for handling the application's BLE stack events.
 *
 * @details This function handles all events from the BLE stack that are of interest to the Configuration Service.
 *
 * @param[in] p_ble_evt  Event received from the BLE stack.
 * @param[in] p_context  LED Button Service structure.
 */
void ble_configs_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);

/**@brief Function for sending a configuration output notification.
 *
 * @param[in] conn_handle   Handle of the peripheral connection to which the button state notification will be sent.
 * @param[in] p_configs     Configuration Service structure.
 * @param[in] new_state     New state.
 *
 * @retval NRF_SUCCESS If the notification was sent successfully. Otherwise, an error code is returned.
 */
uint32_t ble_configs_on_config_out(uint16_t conn_handle, ble_configs_t * p_configs, uint8_t len, uint8_t *data);

#ifdef __cplusplus
}
#endif

#endif // BLE_CONFIGS_H__

/** @} */
