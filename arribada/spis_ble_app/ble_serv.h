/* ble_serv.h - BLE configuration server
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _BLE_SERV_H_

#include <stdint.h>
#include "ble_types.h"

void ble_serv_start(ble_uuid128_t base_uuid,
    void (*app_config_in_handler)(uint8_t len, uint8_t *data),
    void (*app_config_out_handler)(void),
    void (*app_connection_handler)(bool));

void ble_serv_stop(void);

uint32_t ble_send_config_out(uint8_t len, uint8_t *data);

#endif // _BLE_SERV_H_
