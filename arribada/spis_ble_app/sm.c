/* sm.c - Bluetooth SPI slave application state machine
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sdk_config.h"
#include "app_util_platform.h"
#include "app_timer.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_spis.h"
#include "nrf_gpio.h"
#include "boards.h"
#include "app_error.h"
#include <string.h>

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_delay.h"
#include "ble_serv.h"

#include "nRF52x_regs.h"
#include "buffer.h"
#include "sm.h"

/* Static variables */
static sm_state_t m_state = SM_STATE_BOOT;
static uint8_t m_mode = NRF52_MODE_IDLE;
static bool m_spi_xfer_complete = true;

static buffer_t m_rx_data_port;
static uint8_t  m_rx_data[RX_DATA_PORT_SIZE];
static buffer_t m_tx_data_port;
static uint8_t  m_tx_data[TX_DATA_PORT_SIZE];
static uint8_t  m_tx_packet[MAX_PACKET_SIZE];
static uint8_t  m_tx_pending_size = 0;
static uint8_t  m_rx_overflow_count = 0;

static uint32_t m_fw_upgrade_size;
static uint32_t m_fw_upgrade_crc;
static uint8_t m_fw_upgrade_type;

static uint8_t m_int_status = 0;
static uint8_t m_int_enable = 0;

static uint8_t m_error_code = 0;

static uint8_t m_own_uuid[NRF52_REG_SIZE_OWN_UUID] = { 0x00 };
static uint8_t m_target_uuid[NRF52_REG_SIZE_TARGET_UUID] = { 0x00 };

static uint16_t m_beacon_interval = 0;
static uint8_t m_beacon_payload[NRF52_REG_SIZE_BEACON_PAYLOAD];
static uint8_t m_scan_response[NRF52_REG_SIZE_SCAN_RESPONSE];

static uint16_t m_tx_data_length = 0;

/* Static functions */
static uint32_t copy_port_to_linear_buffer(buffer_t *port, uint8_t *linear_buffer, uint32_t len)
{
    uint32_t avail, actual_len = 0;
    uintptr_t src;
    do
    {
        avail = buffer_read(port, &src);
        avail = MIN(avail, len);
        memcpy(linear_buffer, (void *)src, avail);
        buffer_read_advance(port, avail);
        linear_buffer += avail;
        len -= avail;
        actual_len += avail;
    } while (avail > 0 && len > 0);

    return actual_len;
}

static uint32_t copy_linear_buffer_to_port(buffer_t *port, uint8_t *linear_buffer, uint32_t len)
{
    uint32_t avail, actual_len = 0;
    uintptr_t dest;
    do
    {
        avail = buffer_write(port, &dest);
        avail = MIN(avail, len);
        memcpy((void *)dest, linear_buffer, avail);
        buffer_write_advance(port, avail);
        linear_buffer += avail;
        len -= avail;
        actual_len += avail;
    } while (avail > 0 && len > 0);

    return actual_len;
}

static void spi_xfer_complete_callback(void)
{
    m_spi_xfer_complete = true;
}

static uint16_t read_app_version_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    static const uint16_t app_version = 0x0001;
    memcpy(buffer, &app_version, sizeof(app_version));
    NRF_LOG_DEBUG("read_app_version_request: ver=%04x", app_version);
    return sizeof(app_version);
}

static uint16_t read_soft_dev_version_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    static const uint16_t soft_dev_version = 0x0001;
    memcpy(buffer, &soft_dev_version, sizeof(soft_dev_version));
    NRF_LOG_DEBUG("read_soft_dev_version_request: ver=%04x", soft_dev_version);
    return sizeof(soft_dev_version);
}

static uint16_t read_mode_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(buffer, &m_mode, sizeof(m_mode));
    NRF_LOG_DEBUG("read_mode_request: m_mode=%02x", m_mode);
    return sizeof(m_mode);
}

static void write_mode_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    m_mode = buffer[0];
    NRF_LOG_DEBUG("write_mode_complete: m_mode=%02x", m_mode);
}

static void write_fw_upgrade_size_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(&m_fw_upgrade_size, buffer, sizeof(m_fw_upgrade_size));
    NRF_LOG_DEBUG("write_fw_upgrade_size_complete: m_fw_upgrade_size=%08x", m_fw_upgrade_size);
}

static void write_fw_upgrade_crc_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(&m_fw_upgrade_crc, buffer, sizeof(m_fw_upgrade_crc));
    NRF_LOG_DEBUG("write_fw_upgrade_crc_complete: m_fw_upgrade_crc=%08x", m_fw_upgrade_crc);
}

static void write_fw_upgrade_type_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    m_fw_upgrade_type = buffer[0];
    NRF_LOG_DEBUG("write_fw_upgrade_type_complete: m_fw_upgrade_type=%02x", m_fw_upgrade_type);
}

static uint16_t read_int_status_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(buffer, &m_int_status, sizeof(m_int_status));
    NRF_LOG_DEBUG("read_int_status_request: m_int_status=%02x", m_int_status);
    return sizeof(m_int_status);
}

static void write_int_enable_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    m_int_enable = buffer[0];
    NRF_LOG_DEBUG("write_int_enable_complete: m_int_enable=%02x", m_int_enable);
}

static uint16_t read_error_code_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(buffer, &m_error_code, sizeof(m_error_code));
    NRF_LOG_DEBUG("read_error_code_request: m_error_code=%02x", m_error_code);
    return sizeof(m_error_code);
}

static void write_own_uuid_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(m_own_uuid, buffer, sizeof(m_own_uuid));
    NRF_LOG_INFO("write_own_uuid_complete: m_own_uuid=%02x %02x %02x %02x ...",
            m_own_uuid[0],
            m_own_uuid[1],
            m_own_uuid[2],
            m_own_uuid[3]
    );
}

static void write_target_uuid_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(m_target_uuid, buffer, sizeof(m_target_uuid));
    NRF_LOG_DEBUG("write_target_uuid_complete: m_target_uuid=%02x %02x %02x %02x ...",
            m_target_uuid[0],
            m_target_uuid[1],
            m_target_uuid[2],
            m_target_uuid[3]
    );
}

static uint16_t read_target_uuid_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(buffer, m_target_uuid, sizeof(m_target_uuid));
    NRF_LOG_DEBUG("read_target_uuid_request: m_target_uuid=%02x %02x %02x %02x ...",
            m_target_uuid[0],
            m_target_uuid[1],
            m_target_uuid[2],
            m_target_uuid[3]
    );
    return sizeof(m_target_uuid);
}

static void write_beacon_interval_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(&m_beacon_interval, buffer, sizeof(m_beacon_interval));
    NRF_LOG_DEBUG("write_beacon_interval_complete: m_beacon_interval=%04x", m_beacon_interval);
}

static void write_beacon_payload_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(m_beacon_payload, buffer, sizeof(m_beacon_payload));
    NRF_LOG_DEBUG("write_beacon_payload_complete: m_beacon_payload=%02x %02x %02x %02x ...",
            m_beacon_payload[0],
            m_beacon_payload[1],
            m_beacon_payload[2],
            m_beacon_payload[3]
    );
}

static void write_scan_response_complete(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(m_scan_response, buffer, sizeof(m_scan_response));
    NRF_LOG_DEBUG("write_scan_response_complete: m_scan_response=%02x %02x %02x %02x ...",
            m_scan_response[0],
            m_scan_response[1],
            m_scan_response[2],
            m_scan_response[3]
    );
}

static uint16_t read_tx_data_length_request(uint8_t *buffer, uint16_t length)
{
    (void) length;
    memcpy(buffer, &m_tx_data_length, sizeof(m_tx_data_length));
    NRF_LOG_DEBUG("read_tx_data_length_request: m_tx_data_length=%08x", m_tx_data_length);
    return sizeof(m_tx_data_length);
}

static void write_tx_data_port_complete(uint8_t *buffer, uint16_t length)
{
    buffer_t *port;
    uint16_t total_size = 0;

    /* Put packets into RX data port if we are in loopback mode */
    if (m_mode == NRF52_MODE_LOOPBACK)
        port = &m_rx_data_port;
    else
        port = &m_tx_data_port;

    total_size = copy_linear_buffer_to_port(port, buffer, length);

    NRF_LOG_DEBUG("write_tx_data_port_complete: length=%u", total_size);
}

static uint16_t read_rx_data_length_request(uint8_t *buffer, uint16_t length)
{
    uint16_t rx_data_length;
    (void) length;
    rx_data_length = buffer_occupancy(&m_rx_data_port);
    memcpy(buffer, &rx_data_length, sizeof(rx_data_length));
    NRF_LOG_DEBUG("read_rx_data_length_request: rx_data_length=%04x", rx_data_length);
    return sizeof(rx_data_length);
}

static uint16_t read_rx_data_port_request(uint8_t *buffer, uint16_t length)
{
    uint16_t total_size = 0;

    total_size = copy_port_to_linear_buffer(&m_rx_data_port, buffer, length);

    NRF_LOG_DEBUG("read_rx_data_port_complete: length=%04x", total_size);

    return total_size;
}

static void spi_slave_process(void)
{
    if (m_spi_xfer_complete)
    {
        m_spi_xfer_complete = false;
        nRF52_slave_transaction();
    }
}

static void wait_for_activity(void)
{
    NRF_LOG_FLUSH();
    nrf_pwr_mgmt_run();
}

static inline void irq_clear(void)
{
    nrf_gpio_pin_set(API_SPIS_IRQ_PIN);
}

static inline void irq_set(void)
{
    nrf_gpio_pin_clear(API_SPIS_IRQ_PIN);
}

static void irq_init(void)
{
    nrf_gpio_cfg_output(API_SPIS_IRQ_PIN);
    irq_clear();
}

static void irq_process(void)
{
    if (m_int_enable & m_int_status)
    {
        irq_set();
        irq_clear();
    }
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module.
 */
static void timers_init(void)
{
    // Initialize timer module, making it use the scheduler
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

static void power_management_init(void)
{
    ret_code_t ret;
    ret = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(ret);
}

static void hard_reset(void)
{
    nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_RESET);
}

static void gatt_server_config_in_handler(uint8_t len, uint8_t *data)
{
    /* Copy packet into the RX data port */
    NRF_LOG_DEBUG("gatt_server_config_in_handler: len=%u", len);

    (void)copy_linear_buffer_to_port(&m_rx_data_port, data, len);

    /* Handle data port overflow condition */
    uint8_t overflow_count = (uint8_t)buffer_overflows(&m_rx_data_port);
    if (overflow_count != m_rx_overflow_count)
    {
        m_int_status |= NRF52_INT_ERROR_INDICATION;
        m_error_code = NRF52_ERROR_OVERFLOW;
        m_rx_overflow_count = overflow_count;
    }
    else
    {
        /* Raise RX data ready IRQ */
        m_int_status |= NRF52_INT_RX_DATA_READY;
    }

    irq_process();
}

static void service_tx_data_port(void)
{
    /* GATT server processing */
    if (m_int_status & NRF52_INT_GATT_CONNECTED)
    {
        uint8_t to_send = copy_port_to_linear_buffer(&m_tx_data_port, m_tx_packet, 244);
        int cnt = 0;
        if (to_send)
        {
            uint32_t err_code = ble_send_config_out(to_send, m_tx_packet);
            NRF_LOG_INFO("send[%u] = %u", cnt++, err_code);
        }
    }
}

static void gatt_server_config_out_handler(void)
{
    NRF_LOG_DEBUG("gatt_server_config_out_handler");
    /* Last packet was sent */
    m_tx_data_length += m_tx_pending_size;
    m_tx_pending_size = 0;
    m_int_status |= NRF52_INT_TX_DATA_SENT;
    irq_process();
}

static void gatt_server_connection_handler(bool is_connected)
{
    /* Connection state has changed */
    if (is_connected)
        m_int_status |= NRF52_INT_GATT_CONNECTED;
    else
        m_int_status &= ~NRF52_INT_GATT_CONNECTED;
    irq_process();

    // TODO: we need to pass UUID of the connected device into
    // m_target_uuid.  This could be done using a new ble_serv
    // function call.
}

static void start_gatt_server(void)
{
    ble_uuid128_t uuid;

    NRF_LOG_INFO("start_gatt_server");

    memcpy(uuid.uuid128, m_own_uuid, sizeof(m_own_uuid));
    ble_serv_start((ble_uuid128_t)uuid,
        gatt_server_config_in_handler,
        gatt_server_config_out_handler,
        gatt_server_connection_handler);
}

static void stop_gatt_server(void)
{
    ble_serv_stop();
    buffer_reset(&m_tx_data_port);
    buffer_reset(&m_rx_data_port);
    m_int_status = 0;
    m_rx_overflow_count = 0;
    m_error_code = 0;
}

static void cleanup_current_state(void)
{
    /* Add any state-specific clean-up code here */
    switch (m_state)
    {
        case SM_STATE_GATT_SERVER:
        {
            stop_gatt_server();
        } break;
        default:
            break;
    }
}

static void apply_new_state(sm_state_t new_state)
{
    /* Add any state-specific entry code here */
    NRF_LOG_INFO("apply_new_state: %u -> %u", m_state, new_state);
    switch (new_state)
    {
        case SM_STATE_GATT_SERVER:
        {
            start_gatt_server();
        } break;
        default:
            break;
    }

    m_state = new_state;
}

static void transition_state(sm_state_t new_state)
{
    if (new_state == m_state)
        return;

    /* State change has been requested -- check for any clean-up operations */
    cleanup_current_state();

    /* Prepare new state */
    apply_new_state(new_state);
}

static void process_mode_change(void)
{
    /* Check to see if mode has changed */
    switch (m_mode)
    {
        case NRF52_MODE_DEEP_SLEEP:
            transition_state(SM_STATE_DEEP_SLEEP);
            break;
        case NRF52_MODE_BEACON:
            transition_state(SM_STATE_BEACON);
            break;
        case NRF52_MODE_SCAN:
            transition_state(SM_STATE_SCAN);
            break;
        case NRF52_MODE_GATT_CLIENT:
            transition_state(SM_STATE_GATT_CLIENT);
            break;
        case NRF52_MODE_GATT_SERVER:
            transition_state(SM_STATE_GATT_SERVER);
            break;
        case NRF52_MODE_FW_UPGRADE:
            transition_state(SM_STATE_FW_UPGRADE);
            break;
        case NRF52_MODE_RESET:
            hard_reset();
            break;
        case NRF52_MODE_LOOPBACK:
        case NRF52_MODE_IDLE:
        default:
            transition_state(SM_STATE_IDLE);
            break;
    }
}

static void sm_state_gatt_client(void)
{
    spi_slave_process();
    wait_for_activity();
    process_mode_change();

    /* TODO: GATT client processing */
}

static void sm_state_gatt_server(void)
{
    spi_slave_process();
    service_tx_data_port();
    process_mode_change();
    wait_for_activity();
}

static void sm_state_deep_sleep(void)
{
    /* TODO: transition CPU to deep sleep mode */
}

static void sm_state_scan(void)
{
    spi_slave_process();
    wait_for_activity();
    process_mode_change();

    /* TODO: BLE scan processing */
}

static void sm_state_beacon(void)
{
    spi_slave_process();
    wait_for_activity();
    process_mode_change();

    /* TODO: BLE beacon processing */
}

static void sm_state_fw_upgrade(void)
{
    spi_slave_process();
    wait_for_activity();
    process_mode_change();

    /* TODO: FW upgrade processing */

}

static void sm_state_idle(void)
{
    spi_slave_process();
    wait_for_activity();
    process_mode_change();
}

static void sm_state_boot(void)
{
    /* Setup timers */
    timers_init();

    /* Setup power management */
    power_management_init();

    /* Setup register interface abstraction over SPI slave */
    APP_ERROR_CHECK(nRF52_reg_init(spi_xfer_complete_callback));

    /* Install SPI register read/write handlers */
    nRF52_reg_install(NRF52_REG_ADDR_APP_VERSION, read_app_version_request, NULL, NULL);
    nRF52_reg_install(NRF52_REG_ADDR_SOFT_DEV_VERSION, read_soft_dev_version_request, NULL, NULL);
    nRF52_reg_install(NRF52_REG_ADDR_MODE, read_mode_request, NULL,
            write_mode_complete);
    nRF52_reg_install(NRF52_REG_ADDR_FW_UPGRADE_SIZE, NULL, NULL,
            write_fw_upgrade_size_complete);
    nRF52_reg_install(NRF52_REG_ADDR_FW_UPGRADE_TYPE, NULL, NULL,
            write_fw_upgrade_type_complete);
    nRF52_reg_install(NRF52_REG_ADDR_FW_UPGRADE_CRC, NULL, NULL,
            write_fw_upgrade_crc_complete);
    nRF52_reg_install(NRF52_REG_ADDR_INT_STATUS, read_int_status_request, NULL, NULL);
    nRF52_reg_install(NRF52_REG_ADDR_INT_ENABLE, NULL, NULL,
            write_int_enable_complete);
    nRF52_reg_install(NRF52_REG_ADDR_ERROR_CODE, read_error_code_request, NULL, NULL);
    nRF52_reg_install(NRF52_REG_ADDR_OWN_UUID, NULL, NULL,
            write_own_uuid_complete);
    nRF52_reg_install(NRF52_REG_ADDR_TARGET_UUID, read_target_uuid_request, NULL,
            write_target_uuid_complete);
    nRF52_reg_install(NRF52_REG_ADDR_BEACON_INTERVAL, NULL, NULL,
            write_beacon_interval_complete);
    nRF52_reg_install(NRF52_REG_ADDR_BEACON_PAYLOAD, NULL, NULL,
            write_beacon_payload_complete);
    nRF52_reg_install(NRF52_REG_ADDR_SCAN_RESPONSE, NULL, NULL,
            write_scan_response_complete);
    nRF52_reg_install(NRF52_REG_ADDR_TX_DATA_PORT, NULL, NULL,
            write_tx_data_port_complete);
    nRF52_reg_install(NRF52_REG_ADDR_TX_DATA_LENGTH, read_tx_data_length_request, NULL, NULL);
    nRF52_reg_install(NRF52_REG_ADDR_RX_DATA_PORT, read_rx_data_port_request, NULL, NULL);
    nRF52_reg_install(NRF52_REG_ADDR_RX_DATA_LENGTH, read_rx_data_length_request, NULL, NULL);

    /* Setup circular buffer for tx and rx data ports */
    buffer_init_policy(circular, &m_rx_data_port, (uintptr_t)m_rx_data, RX_DATA_PORT_SIZE);
    buffer_init_policy(circular, &m_tx_data_port, (uintptr_t)m_tx_data, TX_DATA_PORT_SIZE);

    /* Setup GPIO for IRQ output */
    irq_init();

    transition_state(SM_STATE_IDLE);
}

void sm_tick(void)
{
    switch (m_state)
    {
        case SM_STATE_BOOT:
            sm_state_boot();
            break;
        case SM_STATE_IDLE:
            sm_state_idle();
            break;
        case SM_STATE_FW_UPGRADE:
            sm_state_fw_upgrade();
            break;
        case SM_STATE_GATT_CLIENT:
            sm_state_gatt_client();
            break;
        case SM_STATE_GATT_SERVER:
            sm_state_gatt_server();
            break;
        case SM_STATE_DEEP_SLEEP:
            sm_state_deep_sleep();
            break;
        case SM_STATE_SCAN:
            sm_state_scan();
            break;
        case SM_STATE_BEACON:
            sm_state_beacon();
            break;
        default:
            break;
    }
}
