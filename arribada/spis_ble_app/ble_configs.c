/**
 * Copyright (c) 2013 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "sdk_common.h"

#if NRF_MODULE_ENABLED(BLE_CONFIGS)

#include "ble_configs.h"
#include "ble_srv_common.h"
#include "nrf_log.h"


/**@brief Function for handling the config in event.
 *
 * @param[in] p_configs  Configuration Service structure.
 * @param[in] p_ble_evt  Event received from the BLE stack.
 */
static void on_config_in(ble_configs_t * p_configs, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if (   (p_evt_write->handle == p_configs->config_in_handles.value_handle)
        && (p_configs->config_in_handler != NULL))
    {
        p_configs->config_in_handler(p_ble_evt->evt.gap_evt.conn_handle, p_configs,
            p_evt_write->len, (uint8_t *)p_evt_write->data);
    }
}

/**@brief Function for handling the config out event.
 *
 * @param[in] p_configs  Configuration Service structure.
 * @param[in] p_ble_evt  Event received from the BLE stack.
 */
static void on_config_out(ble_configs_t * p_configs, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_hvn_tx_complete_t const * p_evt = &p_ble_evt->evt.gatts_evt.params.hvn_tx_complete;

    if (p_evt->count > 0)
        p_configs->config_out_handler(p_ble_evt->evt.gap_evt.conn_handle);
}

void ble_configs_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_configs_t * p_configs = (ble_configs_t *)p_context;

    NRF_LOG_DEBUG("ble_evt: %u", p_ble_evt->header.evt_id);

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GATTS_EVT_WRITE:
            on_config_in(p_configs, p_ble_evt);
            break;
        case BLE_GATTS_EVT_HVN_TX_COMPLETE:
            on_config_out(p_configs, p_ble_evt);
            break;
        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for adding the configuration input characteristic.
 *
 * @param[in] p_configs      Configuration Service structure.
 * @param[in] p_configs_init Configuration Service initialization structure.
 *
 * @retval NRF_SUCCESS on success, else an error value from the SoftDevice
 */
static uint32_t config_in_char_add(ble_configs_t * p_configs, const ble_configs_init_t * p_configs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.p_char_user_desc = NULL;
    char_md.p_char_pf        = NULL;
    char_md.p_user_desc_md   = NULL;
    char_md.p_cccd_md        = NULL;
    char_md.p_sccd_md        = NULL;

    ble_uuid.type = p_configs->uuid_type;
    ble_uuid.uuid = CONFIGS_UUID_CONFIG_IN_CHAR;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 0;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = NRF_SDH_BLE_GATT_MAX_MTU_SIZE;
    attr_char_value.p_value   = NULL;

    return sd_ble_gatts_characteristic_add(p_configs->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_configs->config_in_handles);
}


/**@brief Function for adding the configuration out characteristic.
 *
 * @param[in] p_configs      Configuration Service structure.
 * @param[in] p_configs_init Configuration Service initialization structure.
 *
 * @retval NRF_SUCCESS on success, else an error value from the SoftDevice
 */
static uint32_t config_out_char_add(ble_configs_t * p_configs, const ble_configs_init_t * p_configs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_configs->uuid_type;
    ble_uuid.uuid = CONFIGS_UUID_CONFIG_OUT_CHAR;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&attr_md.write_perm);
    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 0;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = NRF_SDH_BLE_GATT_MAX_MTU_SIZE;
    attr_char_value.p_value   = NULL;

    return sd_ble_gatts_characteristic_add(p_configs->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_configs->config_out_handles);
}

uint32_t ble_configs_init(ble_configs_t * p_configs, const ble_configs_init_t * p_configs_init)
{
    uint32_t   err_code;
    ble_uuid_t ble_uuid;

    // Initialize service structure.
    p_configs->config_out_handler = p_configs_init->config_out_handler;
    p_configs->config_in_handler = p_configs_init->config_in_handler;

    // Add service.
    err_code = sd_ble_uuid_vs_add(&p_configs_init->base_uuid, &p_configs->uuid_type);
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = p_configs->uuid_type;
    ble_uuid.uuid = CONFIGS_UUID_SERVICE;

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_configs->service_handle);
    VERIFY_SUCCESS(err_code);

    // Add characteristics.
    err_code = config_out_char_add(p_configs, p_configs_init);
    VERIFY_SUCCESS(err_code);

    err_code = config_in_char_add(p_configs, p_configs_init);
    VERIFY_SUCCESS(err_code);

    return NRF_SUCCESS;
}


uint32_t ble_configs_on_config_out(uint16_t conn_handle, ble_configs_t * p_configs, uint8_t len, uint8_t *data)
{
    ble_gatts_hvx_params_t params;
    uint16_t len16 = len;

    memset(&params, 0, sizeof(params));
    params.type   = BLE_GATT_HVX_NOTIFICATION;
    params.handle = p_configs->config_out_handles.value_handle;
    params.p_data = data;
    params.p_len  = &len16;

    return sd_ble_gatts_hvx(conn_handle, &params);
}

#endif // NRF_MODULE_ENABLED(BLE_CONFIGS)
