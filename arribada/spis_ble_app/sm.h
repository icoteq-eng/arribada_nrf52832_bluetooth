/* sm.c - Bluetooth SPI slave application state machine
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _SM_H_
#define _SM_H_

#define MAX_PACKET_SIZE             256
#define RX_DATA_PORT_SIZE           (MAX_PACKET_SIZE * RX_DATA_PORT_NUM_BUFFERS)
#define RX_DATA_PORT_NUM_BUFFERS    8

#define TX_DATA_PORT_SIZE           (MAX_PACKET_SIZE * TX_DATA_PORT_NUM_BUFFERS)
#define TX_DATA_PORT_NUM_BUFFERS    8

typedef enum
{
    SM_STATE_BOOT,
    SM_STATE_IDLE,
    SM_STATE_FW_UPGRADE,
    SM_STATE_BEACON,
    SM_STATE_SCAN,
    SM_STATE_GATT_CLIENT,
    SM_STATE_GATT_SERVER,
    SM_STATE_DEEP_SLEEP
} sm_state_t;

void sm_tick(void);

#endif //_SM_H_
