/* arribada_v1.h - Board definitions
 *
 * Copyright (C) 2018 Arribada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ARRIBADA_V1_H
#define ARRIBADA_V1_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

// LEDs definitions for PCA10040
#define BSP_SIMPLE

#define LEDS_NUMBER    0

#define BUTTONS_NUMBER 0

#define SPIS_MISO_PIN       4   // SPI MISO signal.
#define SPIS_CSN_PIN        5   // SPI CSN signal.
#define SPIS_MOSI_PIN       3   // SPI MOSI signal.
#define SPIS_SCK_PIN        2   // SPI SCK signal.
#define SPIS_IRQ_PIN        29  // SPI IRQ signal.

// Debug UART
#define DEBUG_APP_TX_PIN    6    // UART TX pin number.

#ifdef __cplusplus
}
#endif

#endif // ARRIBADA_V1_H
